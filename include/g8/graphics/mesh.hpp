#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <vector>

#include "texture.hpp"

namespace mesh
{
    struct m_static
    {
        std::string name;

        // STORED: vertex (3 floats), normal (3 floats), tex coord (2 floats)
        GLfloat* vertices;
        GLuint*  indices;

        int vertex_size;

        // gl data
        GLuint vertex_buffer_id;
        GLuint index_buffer_id;

        // transform data
        glm::fvec3 position = glm::fvec3(0.0f);
        glm::fvec3 scale    = glm::fvec3(1.0f);
        glm::fquat rotation = glm::fquat(0.0f, 0.0f, 0.0f, 0.0f);
    };

    struct m_dynamic
    {
        std::string name;

        // STORED: vertex (3 floats), normal (3 floats), tex coord (2 floats)
        std::vector<GLfloat> vertices;
        std::vector<GLuint>  indices;

        // gl data
        GLuint vertex_buffer_id;
        GLuint index_buffer_id;

        // transform data
        glm::fvec3 position = glm::fvec3(0.0f);
        glm::fvec3 scale    = glm::fvec3(1.0f);
        glm::fquat rotation = glm::fquat(0.0f, 0.0f, 0.0f, 0.0f);
    };

    glm::fmat4 get_model_matrix(m_static* msh)
    {
        glm::fmat4 pmat = glm::translate(glm::fmat4(1.0f), msh->position);
        glm::fmat4 rmat = glm::mat4_cast(msh->rotation);
        glm::fmat4 smat = glm::scale(glm::fmat4(1.0f), msh->scale);

        return pmat * rmat * smat;
    }

    void buffer_verts(m_static* msh, GLfloat verts[], size_t size, GLenum usage)
    {
        glGenBuffers(1, &(msh->vertex_buffer_id));
        glBindBuffer(GL_ARRAY_BUFFER, msh->vertex_buffer_id);
        glBufferData(GL_ARRAY_BUFFER, size, verts, usage);
    }

    void buffer_indices(m_static* msh, GLuint indices[], size_t size, GLenum usage)
    {
        glGenBuffers(1, &(msh->index_buffer_id));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, msh->index_buffer_id);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, usage);
    }
}
