#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "mesh.hpp"

namespace renderer
{
    struct r_standard
    {
        GLuint program_id;

        r_standard(GLuint p_id)
        {
            program_id = p_id;
        }

        void render(mesh::m_static* msh, glm::mat4 projection_matrix, glm::mat4 view_matrix)
        {
            glm::mat4 mvp = projection_matrix * view_matrix * mesh::get_model_matrix(msh);

            glBindBuffer(GL_ARRAY_BUFFER, msh->vertex_buffer_id);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, msh->index_buffer_id);

            // need more attributes? remember to enable them!
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

            glUseProgram(program_id);

            // pass mvp
            GLuint matrix_id = glGetUniformLocation(program_id, "u_mvp");
            glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &mvp[0][0]);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*) 0);
        }
    };
}
