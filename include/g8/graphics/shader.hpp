#pragma once

#include "../io/io.hpp"

namespace shader
{
    // returns if true if error
    bool load(GLuint shader_id, std::string path)
    {
        std::string shader_code;
        if (!io::file::to_string(shader_code, io::file::make_relative(path)))
            return true;

        char const* src_ptr = shader_code.c_str();

        glShaderSource(shader_id, 1, &src_ptr, NULL);
        glCompileShader(shader_id);

        return false;
    }

    bool is_okay(GLuint shader_id)
    {
        GLint Result = GL_FALSE;
        int InfoLogLength;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &Result);
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
        // TODO check if this test is right
        if (InfoLogLength > 1)
        {
            std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
            glGetShaderInfoLog(shader_id, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
            printf("%s\n", &VertexShaderErrorMessage[0]);
            return false;
        }

        return true;
    }
}
