#pragma once

#include <GL/glew.h>

#include "../io/io.hpp"

#include "shader.hpp"

namespace program
{
    bool is_okay(GLuint program_id)
    {
        GLint Result = GL_FALSE;

        int InfoLogLength;

        glGetProgramiv(program_id, GL_LINK_STATUS, &Result);
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &InfoLogLength);
        
        if (InfoLogLength > 1)
        {
            std::vector<char> ProgramErrorMessage(InfoLogLength+1);
            glGetProgramInfoLog(program_id, InfoLogLength, NULL, &ProgramErrorMessage[0]);
            printf("%s\n", &ProgramErrorMessage[0]);
            return false;
        }

        return true;
    }

    GLuint create(std::string vertex_file_path, std::string fragment_file_path)
    {
        // Create the shaders
        GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        // Load & compile shaders
        if(shader::load(VertexShaderID, vertex_file_path))
            std::cout << "error loading vert shader" << std::endl;

        if(shader::load(FragmentShaderID, fragment_file_path))
            std::cout << "error loading frag shader" << std::endl;

        // Check shaders
        if(!shader::is_okay(VertexShaderID)) std::cout << "vert shader is not okay." << std::endl;
        if(!shader::is_okay(FragmentShaderID)) std::cout << "frag shader is not okay." << std::endl;

        // Link the program
        printf("Linking program\n");
        GLuint program_id = glCreateProgram();
        glAttachShader(program_id, VertexShaderID);
        glAttachShader(program_id, FragmentShaderID);
        glLinkProgram(program_id);

        // Check the program
        if (!is_okay(program_id)) std::cout << "oof. program is not okay." << std::endl;

        glDetachShader(program_id, VertexShaderID);
        glDetachShader(program_id, FragmentShaderID);

        // delete the shaders
        glDeleteShader(VertexShaderID);
        glDeleteShader(FragmentShaderID);

        return program_id;
    }
}
