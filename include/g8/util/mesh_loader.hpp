#pragma once

#include <iostream>
#include <vector>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <unordered_map>

#define TINYOBJLOADER_IMPLEMENTATION
#include <syoyo/tiny_obj_loader.h>

#include "../io/io.hpp"
#include "../graphics/mesh.hpp"
#include "console.hpp"

namespace mesh_loader
{
    const int SUCCESS = 0;
    const int FILE_NOT_FOUND = 1;
    const int PARSE_ERROR = 2;

    struct parse_object
    {
        std::string name;

        std::vector<GLfloat> vertices;
        std::vector<GLfloat> normals;
        std::vector<GLfloat> texcoords;

        std::vector<GLuint> v_indices;
        std::vector<GLuint> vn_indices;
        std::vector<GLuint> vt_indices;

        std::vector<int> material_ids;
    };

    struct parse_state
    {
        std::vector<tinyobj::material_t> materials;

        std::vector<parse_object*> objects;

        parse_object* current = NULL;
    };

    void vertex_cb(void *user_data, float x, float y, float z, float w)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        p_state->current->vertices.push_back(x);
        p_state->current->vertices.push_back(y);
        p_state->current->vertices.push_back(z);
    }

    void normal_cb(void *user_data, float x, float y, float z)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        p_state->current->normals.push_back(x);
        p_state->current->normals.push_back(y);
        p_state->current->normals.push_back(z);
    }

    void texcoord_cb(void *user_data, float x, float y, float z)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        p_state->current->texcoords.push_back(x);
        p_state->current->texcoords.push_back(y);
        p_state->current->texcoords.push_back(z);
    }

    void face_cb(void *user_data, tinyobj::index_t *indices, int num_indices)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        for (int i = 0; i < num_indices; i++)
        {
            tinyobj::index_t idx = indices[i];

            if (idx.vertex_index != 0)
                p_state->current->v_indices.push_back(idx.vertex_index - 1);

            if (idx.normal_index != 0)
                p_state->current->vn_indices.push_back(idx.normal_index - 1);

            if (idx.texcoord_index != 0)
                p_state->current->vt_indices.push_back(idx.texcoord_index - 1);
        }
    }

    void usemtl_cb(void *user_data, const char *name, int material_idx)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        if ((material_idx > -1) && (material_idx < p_state->materials.size()))
            p_state->current->material_ids.push_back(material_idx);
    }

    void mtllib_cb(void *user_data, const tinyobj::material_t *materials, int num_materials)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        for (int i = 0; i < num_materials; i++)
            p_state->materials.push_back(materials[i]);
    }

    void group_cb(void *user_data, const char **names, int num_names)
    {
        // we ignore groups and load everything flat
    }

    void object_cb(void *user_data, const char *name)
    {
        parse_state* p_state = reinterpret_cast<parse_state*>(user_data);

        if (p_state->current != NULL)
            p_state->objects.push_back(p_state->current);

        p_state->current = new parse_object();
        p_state->current->name = std::string(name);
    }

    int import(
        std::string obj_path,
        std::string mat_path,
        std::vector<mesh::m_dynamic*>* meshes
    )
    {
        tinyobj::callback_t cb;

        cb.vertex_cb = vertex_cb;
        cb.normal_cb = normal_cb;
        cb.texcoord_cb = texcoord_cb;
        cb.index_cb = face_cb;
        cb.usemtl_cb = usemtl_cb;
        cb.mtllib_cb = mtllib_cb;
        cb.group_cb = group_cb;
        cb.object_cb = object_cb;

        std::ifstream ifs(obj_path.c_str());

        if (ifs.fail()) return FILE_NOT_FOUND;

        tinyobj::MaterialFileReader mtlReader(mat_path.c_str());

        parse_state p_state;

        std::string warn;
        std::string err;

        bool parsed = tinyobj::LoadObjWithCallback(ifs, cb, &p_state, &mtlReader, &warn, &err);

        if (!warn.empty()) std::cout << "WARN: " << warn << std::endl;
        if (!err.empty()) std::cerr << err << std::endl;
        if (!parsed) return PARSE_ERROR;

        p_state.objects.push_back(p_state.current);

        for (auto p_obj : p_state.objects)
        {
            mesh::m_dynamic* msh = new mesh::m_dynamic();
            msh->name = p_obj->name;

            uint32_t index;

            std::unordered_map<std::string, uint32_t> vert_map;

            bool textured = (p_obj->texcoords.size() > 0);

            for (int i = 0; i < p_obj->v_indices.size(); i += 3)
            {
                GLuint x_vert_index = p_obj->v_indices[i    ];
                GLuint y_vert_index = p_obj->v_indices[i + 1];
                GLuint z_vert_index = p_obj->v_indices[i + 2];

                GLuint x_norm_index = p_obj->vn_indices[i    ];
                GLuint y_norm_index = p_obj->vn_indices[i + 1];
                GLuint z_norm_index = p_obj->vn_indices[i + 2];

                GLuint x_tex_index, y_tex_index;
                if (textured)
                {
                    x_tex_index  = p_obj->vt_indices[i    ];
                    y_tex_index  = p_obj->vt_indices[i + 1];
                }

                std::string tag;
                if (textured)
                {
                    tag = std::to_string(x_vert_index)
                        + std::to_string(y_vert_index)
                        + std::to_string(z_vert_index)
                        + std::to_string(x_norm_index)
                        + std::to_string(y_norm_index)
                        + std::to_string(z_norm_index)
                        + std::to_string(x_tex_index )
                        + std::to_string(y_tex_index );
                }
                else
                {
                    tag = std::to_string(x_vert_index)
                        + std::to_string(y_vert_index)
                        + std::to_string(z_vert_index)
                        + std::to_string(x_norm_index)
                        + std::to_string(y_norm_index)
                        + std::to_string(z_norm_index);
                }

                auto ret = vert_map.insert(std::pair<std::string, int>(tag, index));

                // if this vert already exists, push the index it has
                if (ret.second == false)
                    msh->indices.push_back(ret.first->second);

                // add new vert and increment vertices
                else
                {
                    msh->vertices.push_back(p_obj->vertices[x_vert_index]);
                    msh->vertices.push_back(p_obj->vertices[y_vert_index]);
                    msh->vertices.push_back(p_obj->vertices[z_vert_index]);

                    msh->vertices.push_back(p_obj->normals[x_norm_index]);
                    msh->vertices.push_back(p_obj->normals[y_norm_index]);
                    msh->vertices.push_back(p_obj->normals[z_norm_index]);

                    if (textured)
                    {
                        msh->vertices.push_back(p_obj->texcoords[x_tex_index]);
                        msh->vertices.push_back(p_obj->texcoords[y_tex_index]);
                    }

                    msh->indices.push_back(index);
                    index++;
                }
            }

            meshes->push_back(msh);
        }

        return SUCCESS;
    }
}
