CYAN='\033[0;36m'
RED='\033[0;31m'
NC='\033[0m'

rm -rf bin/g8pc_x32
rm -rf bin/g8pc_x64

clear
clear

if [ "$(uname -s)" = "Linux" ]
then
    echo -e "${CYAN}Detected LINUX ${NC}"

    if [ "$(uname -m)" = "x86_64" ]
    then
        echo -e "${RED}Detected 64BIT. Building... ${NC}"
        g++ src/* -I include/ -Llibs/ -o bin/g8pc_x64 -Wall -Wextra -pedantic -std=c++11 -m64 -w -DGLEW_STATIC -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -lm -lglfw -lGLEW

    else
        echo -e "${RED}Detected 32BIT. Building... ${NC}"
        g++ src/* -I include/ -Llibs/ -o bin/g8pc_x32 -Wall -Wextra -pedantic -std=c++11 -m32 -w -DGLEW_STATIC -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -lrt -lm -lglfw -lGLEW

    fi

else
    echo -e "${CYAN}Detected NOT LINUX. Assuming WINDOWS ${NC}"

    if [ "$(uname -m)" = "x86_64" ]
    then
        echo -e "${RED}Detected 64BIT. Building... ${NC}"
        g++ src/* -I include/ -Llibs/ -o bin/g8pc_x64 -Wall -Wextra -pedantic -std=c++11 -lws2_32 -m64 -w -DGLEW_STATIC -lglfw3 -lglew32 -lws2_32 -lgdi32 -lopengl32

    else
        echo -e "${RED}Detected 32BIT. Building... ${NC}"
        g++ src/* -I include/ -Llibs/ -o bin/g8pc_x32 -Wall -Wextra -pedantic -std=c++11 -lws2_32 -m32 -w -DGLEW_STATIC -lglfw3 -lglew32 -lws2_32 -lgdi32 -lopengl32

    fi
fi
