#include <stdio.h>
#include <chrono>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "g8/globals.hpp"
#include "g8/graphics/program.hpp"
#include "g8/graphics/mesh.hpp"
#include "g8/graphics/texture.hpp"
#include "g8/graphics/renderer.hpp"
#include "g8/io/io.hpp"
#include "g8/util/console.hpp"
#include "g8/util/mesh_loader.hpp"
#include "g8/audio/audio.hpp"
#include "g8/input/input.hpp"

constexpr std::chrono::nanoseconds timestep(16000000);

int main(int argc, char *argv[])
{
    // Init io
    io::init();

    console::enable_file();
    console::log("initialising...");

    // Initialise GLFW
    if(!glfwInit()) return -1;

    // Open a window and create its OpenGL context
    GLFWwindow* window; // (In the accompanying source code, this variable is global for simplicity)
    window = glfwCreateWindow(640, 480, "G8", NULL, NULL);
    if(!window) {
        glfwTerminate();
        return -1;
    }

    /* make the window's context current */
    glfwMakeContextCurrent(window);

    // init GLEW
    if (glewInit() != GLEW_OK) {
        return -1;
    }

    // init engine components (other than io)
    audio::init();
    input::init(window);

    console::log("initialised.");

    GLuint prog = program::create(
        "res\\vert.shader",
        "res\\frag.shader"
    );

    renderer::r_standard* default_renderer = new renderer::r_standard(prog);

    std::vector<mesh::m_dynamic*> meshes;

    std::cout << "RES: " << mesh_loader::import(
        io::file::make_relative("test.obj"),
        io::file::make_relative(""),
        &meshes
    ) << std::endl;

    std::cout << "LOADED: " << meshes.size() << std::endl;

    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    glm::mat4 projection_matrix = glm::perspective(glm::radians(45.0f), (float) 640 / (float) 480, 0.1f, 100.0f);

    // Camera matrix
    glm::mat4 view_matrix = glm::lookAt(
        glm::vec3(4, 3, 3), // Camera is at (4, 3, 3), in World Space
        glm::vec3(0, 0, 0), // and looks at the origin
        glm::vec3(0, 1, 0)  // Head is up (set to 0, -1, 0 to look upside-down)
    );

    // RENDER LOOP
    float delta_time = 0.0f;

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    while (glfwWindowShouldClose(window) == 0 && !g::should_exit)
    {
        auto start = std::chrono::high_resolution_clock::now();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // default_renderer->render(test, projection_matrix, view_matrix);

        glfwSwapBuffers(window);
        glfwPollEvents();

        auto end = std::chrono::high_resolution_clock::now();
        auto length_ns = end - start;
        auto wait_ns = timestep - length_ns;

        std::this_thread::sleep_for(wait_ns);
        delta_time = (wait_ns + length_ns).count() / 1000000000.0;
    }

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

    glfwTerminate();

    return 0;
}
